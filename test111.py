import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


driver = webdriver.Chrome(executable_path="Drivers/chromedriver")
driver.maximize_window()
driver.get("https://angular.realworld.io/")
driver.implicitly_wait(2)

##create account
Signupbtn = driver.find_element_by_xpath("/html/body/app-root/app-layout-header/nav/div/ul/li[3]/a")
Signupbtn.click()

usernametxtbx = driver.find_element_by_xpath("/html/body/app-root/app-auth-page/div/div/div/div/form/fieldset/fieldset[1]/input")
usernametxtbx.send_keys("dan242")


mailtxtbox = driver.find_element_by_xpath("/html/body/app-root/app-auth-page/div/div/div/div/form/fieldset/fieldset[2]/input")
mailtxtbox.send_keys("danielmedina242@gmail.com")

passwordtxtbox = driver.find_element_by_xpath("/html/body/app-root/app-auth-page/div/div/div/div/form/fieldset/fieldset[3]/input")
passwordtxtbox.send_keys("12345678")

loginbtn = driver.find_element_by_xpath("/html/body/app-root/app-auth-page/div/div/div/div/form/fieldset/button")
loginbtn.click()
time.sleep(3)

##Logout
Settingsbtn = driver.find_element_by_xpath("/html/body/app-root/app-layout-header/nav/div/ul/li[3]/a")
Settingsbtn.click()

logoutbtn = driver.find_element_by_xpath("/html/body/app-root/app-settings-page/div/div/div/div/button")
logoutbtn.click()
time.sleep(2)

#Signin
signbtn = driver.find_element_by_xpath("/html/body/app-root/app-layout-header/nav/div/ul/li[2]/a")
signbtn.click()

Usertxtbc = driver.find_element_by_xpath("/html/body/app-root/app-auth-page/div/div/div/div/form/fieldset/fieldset[2]/input")
Usertxtbc.send_keys("danielmedina242@gmail.com")

Usertxtps = driver.find_element_by_xpath("/html/body/app-root/app-auth-page/div/div/div/div/form/fieldset/fieldset[3]/input")
Usertxtps.send_keys("12345678")

loginbtn = driver.find_element_by_xpath("/html/body/app-root/app-auth-page/div/div/div/div/form/fieldset/button")
loginbtn.click()